/*9:*/
/*2:*/
package javaTeX;

import java.awt.*;
import java.io.*;
import java.util.*;
import javaTeX.*;/*:2*/

public class memoryword{
/*10:*/
  final static int maxHalfword= 1073741823;

  int lft;
  int rgt;

final void setInt(int Int){
  rgt = Int;
}

void copy(memoryword m){
  lft = m.lft;
  rgt = m.rgt;
}

final void setlh(int lh){
  lft = lh;
}

final void setrh(int rh){
  rgt = rh;
}

final void setb0(int b){
  b&= 0xffff;
  lft&= 0x0000ffff;
  lft|= (b<<16);
}

final void setb1(int b){
  b&= 0xffff;
  lft&= 0xffff0000;
  lft|= b;
}

final void setb2(int b){
  b&= 0xffff;
  rgt&= 0x0000ffff;
  rgt|= (b<<16);
}

final void setb3(int b){
  b&= 0xffff;
  rgt&= 0xffff0000;
  rgt|= b;
}

final void setglue(double g){
  rgt = Float.floatToIntBits((float)g);
}

final void sethh(twohalves hh){
  lft = hh.lh;
  rgt = hh.rh;
}

final void setqqqq(fourquarters qqqq){
  lft= (qqqq.b0<<16)|(qqqq.b1&0x0000ffff);
  rgt= (qqqq.b2<<16)|(qqqq.b3&0x0000ffff);
}

final int getInt(){
  return rgt;
}

final int getlh(){
  return lft;
}

final int getrh(){
  return rgt;
}

final int getb0(){
  return lft>>>16;
}

final int getb1(){
  return lft&0x0000ffff;
}

final int getb2(){
  return rgt>>>16;
}

final int getb3(){
  return rgt&0x0000ffff;
}

final double getglue(){
  return (double)Float.intBitsToFloat(rgt);
}

final twohalves hh(){
twohalves val= new twohalves();
val.lh= lft;
val.rh= rgt;
return val;
}

final fourquarters qqqq(){
fourquarters val= new fourquarters();
val.b0= lft>>>16;
val.b1= lft&0x0000ffff;
val.b2= rgt>>>16;
val.b3= rgt&0x0000ffff;
return val;
}

final void memdump(wordout fmtfile)throws IOException{
fmtfile.writeInt(lft);
fmtfile.writeInt(rgt);
}

final void memundump(wordfile fmtfile)throws IOException{
lft= fmtfile.readInt();
rgt= fmtfile.readInt();
}/*:10*/



}/*:9*/
