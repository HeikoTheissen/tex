@x
@d othercases == others: {default for cases not listed explicitly}
@y
@d othercases == else {default for cases not listed explicitly}
@z

@x
@!text_file=packed file of text_char;
@y
@!text_file=text;
@z

@x
@ Terminal output is done by writing on file |term_out|, which is assumed to
consist of characters of type |text_char|:
@^system dependencies@>

@d print(#)==write(term_out,#) {`|print|' means write on the terminal}
@d print_ln(#)==write_ln(term_out,#) {`|print|' and then start new line}
@d new_line==write_ln(term_out) {start new line}
@d print_nl(#)==  {print information starting on a new line}
  begin new_line; print(#);
  end

@<Globals...@>=
@!term_out:text_file; {the terminal as an output file}
@y
@ Terminal output is done by writing on file |term_out|, which is assumed to
consist of characters of type |text_char|:
@^system dependencies@>

In ISO Pascal, |@!term_out| cannot be defined as a typed file,
because that would prevent |write_ln|. Instead, we define it
below as macro for |output|.

@d print(#)==write(term_out,#) {`|print|' means write on the terminal}
@d print_ln(#)==write_ln(term_out,#) {`|print|' and then start new line}
@d new_line==write_ln(term_out) {start new line}
@d print_nl(#)==  {print information starting on a new line}
  begin new_line; print(#);
  end
@d term_out==OUTPUT {the terminal as an output file}
@z

@x
rewrite(term_out,'TTY:'); {send |term_out| output to the terminal}
@y
rewrite(term_out); {send |term_out| output to the terminal}
@z

@x
@d update_terminal == break(term_out) {empty the terminal output buffer}
@y
@d update_terminal == flush(term_out) {empty the terminal output buffer}
@z

@x
begin reset(web_file); reset(change_file);
@y
begin
 assign(web_file,paramstr(1)+'.web'); reset(web_file);
 assign(change_file,paramstr(2)+'.ch'); reset(change_file);
@z

@x
rewrite(tex_file);
@y
assign(tex_file,paramstr(1)+'.tex'); rewrite(tex_file);
@z

@x
@t\4\4@>{here files should be closed if the operating system requires it}
@y
CLOSE(web_file);CLOSE(change_file);CLOSE(tex_file);
@z
