@x
@!tfm_file:packed file of 0..255;
@y
@!tfm_file:packed file of byte;
@z

@x
reset(tfm_file);
@y
assign(tfm_file,paramstr(1)+'.tfm'); reset(tfm_file);
@z

@x
rewrite(pl_file);
@y
assign(pl_file,paramstr(1)+'.pl'); rewrite(pl_file);
@z

@x
read(tfm_file,tfm[0]);
if tfm[0]>127 then abort('The first byte of the input file exceeds 127!');
@.The first byte...@>
if eof(tfm_file) then abort('The input file is only one byte long!');
@.The input...one byte long@>
read(tfm_file,tfm[1]); lf:=tfm[0]*@'400+tfm[1];
@y
tfm[0]:=tfm_file^;
if tfm[0]>127 then abort('The first byte of the input file exceeds 127!');
@.The first byte...@>
if eof(tfm_file) then abort('The input file is only one byte long!');
@.The input...one byte long@>
get(tfm_file);tfm[1]:=tfm_file^; lf:=tfm[0]*@'400+tfm[1];
@z

@x
  begin if eof(tfm_file) then
    abort('The file has fewer bytes than it claims!');
@.The file has fewer bytes...@>
  read(tfm_file,tfm[tfm_ptr]);
@y
  begin if eof(tfm_file) then
    abort('The file has fewer bytes than it claims!');
@.The file has fewer bytes...@>
  get(tfm_file);tfm[tfm_ptr]:=tfm_file^;
@z

@x
final_end:end.
@y
close(tfm_file); close(pl_file);
final_end:end.
@z
