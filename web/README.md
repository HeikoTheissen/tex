This folder contains minimalistic change files for compiling TeX in ISO Pascal on Windows.
It was originally meant only for use by the author himself, but perhaps this README
helps others benefit from it.
More details are given in [this TeX Stack Exchange post](https://tex.stackexchange.com/a/733920/255231).

Together with packages from the Comprehensive TeX Archive Network (CTAN) these files can be used
to produce TeX executables and a printable version of
the TeX program. The mechanism is described [here](https://mirrors.ctan.org/info/knuth/webman.pdf)
in detail. Here is a summary for those who just want a running TeX program.

## Produce Windows executables for TeX and INITEX

- Install the FreePascalCompiler (`fpc`) on Windows.
- Download Knuth's original [`tex.web`](https://mirrors.ctan.org/systems/knuth/dist/tex/tex.web) from
  CTAN.
- Download the files [`tangle.p`](tangle.p), [`tex.ch`](tex.ch), [`texext.p`](texext.p) from this folder.
- Execute the following commands in the Windows folder where you downloaded the files
  ```batch
  fpc -Miso tangle.p
  tangle tex tex
  fpc -Miso -dinitex -oinitex.exe tex.p
  fpc -Miso tex.p
  fpc texext.p
  ```
- Move the file `tex.pool` to `/tex/bin/tex.pool`. (Create the Windows folders as necessary.)
- Move the executables `initex.exe`, `tex.exe`, and `texext.dll` to a Windows folder that appears in
  the Windows `PATH` environment variable.

## Produce TeX format file for plain TeX

- Download the TeX library source files from [this CTAN package](https://www.ctan.org/pkg/base) into the Windows folder `/tex/lib`.
- Download the Computer Modern font metric files from [this CTAN package](https://www.ctan.org/pkg/cm-tfm) into the Windows folder `/tex/fonts`.
- Execute the command `initex plain`.
  ```
  This is TeX, Version 3.141592653 ISO Pascal (INITEX)
  (/tex/lib/plain.tex Preloading the plain format: codes, registers, parameters,
  fonts, more fonts, macros, math definitions, output routines, hyphenation
  (/tex/lib/hyphen.tex))
  *
  ```
- At the final asterisk, type `\dump` and press ENTER.
- Move the resulting `plain.fmt` file to `/tex/fmt/plain.fmt`.
- Now you can start plain TeX with the command `tex "&plain"`. (This does not use the format preload mechanism.)

## Further processing of DVI output

`tex.exe` produces a DVI output file, which you can process further in many ways
that are beyond the scope of this work.

_One_ option is to convert DVI to PDF with the [`dvipdfm`](https://ctan.org/pkg/dvipdfm) program
(see its [documentation](https://mirrors.ctan.org/dviware/dvipdfm/dvipdfm.pdf)).
For the Computern Modern fonts used by plain TeX to be included in PDF, `dvipdfm`
needs the `*.pfb` font files from [this CTAN folder](https://ctan.org/tex-archive/fonts/amsfonts/pfb).

## Usage of tangle

- The tangle program is invoked as
  ```
  tangle «web file» «change file»
  ```
  where `«web file»` and `«change file»` are without their file extensions `.web`
  and `.ch`, respectively.
- It produces a Pascal source file named `«web file».p` and a (possibly empty) string pool file
  named `«web file».pool`.
- Invoking tangle differently may lead to runtime errors.
  (The minimalistic change file [`tangle.ch`](tangle.ch) that was used to produce `tangle.p` makes no efforts
  to catch or warn about such errors.)

## Usage of weave

- `weave.exe` can be produced from [`weave.web`](https://mirrors.ctan.org/systems/knuth/dist/tex/weave.web) and [`weave.ch`](weave.ch) in the same way
  as `tex.exe` is produced from `tex.web` and `tex.ch`:
  ```
  tangle weave weave
  fpc -Miso weave.p
  ```
- The weave program is invoked with same arguments as the tangle program:
  ```
  weave «web file» «change file»
  ```
- It produces a TeX source file named `«web file».tex` that can be processed with
  plain TeX (using the library [`webmac.tex`](https://mirrors.ctan.org/systems/knuth/dist/lib/webmac.tex))
  to obtain a printable version of the web file.
