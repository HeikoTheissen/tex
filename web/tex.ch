@x
@d banner=='This is TeX, Version 3.141592653' {printed when \TeX\ starts}
@y
@d banner=='This is TeX, Version 3.141592653 ISO Pascal' {printed when \TeX\ starts}
@z

@x
@d debug==@{ {change this to `$\\{debug}\equiv\null$' when debugging}
@d gubed==@t@>@} {change this to `$\\{gubed}\equiv\null$' when debugging}
@y
@d debug==@{$IFDEF debugging@} {compile with \.{-ddebugging} when debugging}
@d gubed==@{$ENDIF@}
@z

@x
@d stat==@{ {change this to `$\\{stat}\equiv\null$' when gathering
  usage statistics}
@d tats==@t@>@} {change this to `$\\{tats}\equiv\null$' when gathering
  usage statistics}
@y
@d stat==@{$IFDEF stats@} {compile with \.{-dstats} when gathering
  usage statistics}
@d tats==@{$ENDIF@}
@z

@x
@d init== {change this to `$\\{init}\equiv\.{@@\{}$' in the production version}
@d tini== {change this to `$\\{tini}\equiv\.{@@\}}$' in the production version}
@y
@d init==@{$IFDEF initex@} {compile with \.{-dinitex} to get \.{INITEX}}
@d tini==@{$ENDIF@}
@z

@x
@d othercases == others: {default for cases not listed explicitly}
@y
@d othercases == else {default for cases not listed explicitly}
@z

@x
@!mem_max=30000; {greatest index in \TeX's internal |mem| array;
@y
@!mem_max=1000000; {greatest index in \TeX's internal |mem| array;
@z

@x
@!font_max=75; {maximum internal font number; must not exceed |max_quarterword|
@y
@!font_max=100; {maximum internal font number; must not exceed |max_quarterword|
@z

@x
@!font_mem_size=20000; {number of words of |font_info| for all fonts}
@y
@!font_mem_size=50000; {number of words of |font_info| for all fonts}
@z

@x
@!max_strings=3000; {maximum number of strings; must not exceed |max_halfword|}
@!string_vacancies=8000; {the minimum number of characters that should be
@y
@!max_strings=6000; {maximum number of strings; must not exceed |max_halfword|}
@!string_vacancies=16000; {the minimum number of characters that should be
@z

@x
@!pool_size=32000; {maximum number of characters in strings, including all
@y
@!pool_size=1000000; {maximum number of characters in strings, including all
@z

@x
@!save_size=600; {space for saving values outside of current group; must be
@y
@!save_size=1800; {space for saving values outside of current group; must be
@z

@x
@!trie_size=8000; {space for hyphenation patterns; should be larger for
@y
@!trie_size=100000; {space for hyphenation patterns; should be larger for
@z

@x
@!file_name_size=40; {file names shouldn't be longer than this}
@!pool_name='TeXformats:TEX.POOL                     ';
@y
@!file_name_size=80; {file names shouldn't be longer than this}
@!pool_name='/tex/bin/tex.pool                       ';
@z

@x
@d mem_top==30000 {largest index in the |mem| array dumped by \.{INITEX};
@y
@d mem_top==1000000 {largest index in the |mem| array dumped by \.{INITEX};
@z

@x
for i:=@'177 to @'377 do xchr[i]:=' ';
@y
for i:=@'177 to @'377 do xchr[i]:=chr(i);
@z

@x
@!alpha_file=packed file of text_char; {files that contain textual data}
@y
@!alpha_file=text_file; {files that contain textual data, untyped}
@z

@x
|name_of_file| could be opened.

@d reset_OK(#)==erstat(#)=0
@d rewrite_OK(#)==erstat(#)=0

@p function a_open_in(var f:alpha_file):boolean;
@y
|name_of_file| could be opened.

ISO Pascal does not accept a file name in its |reset| and |rewrite| statement,
but has a separate |assign| statement for that. We combine this with an
externally implemented |file_search| function, which expands its parameter
|name_of_file|, searching subdirectories if |name_of_file| includes a \.{//}.
If |name_of_file| does not start with a \.{/}, it is interpreted relative to
|name_of_job_area|. |file_search| returns an empty string if
the file is not found, which means |name_of_file[1]=chr(0)| in the packed array.

Writing is simpler, because it does not involve a file search.

@d assign_read(#)==name_of_file:=file_search(name_of_job_area,name_of_file,name_length);
  name_length:=pos(chr(0),name_of_file)-1;
  if name_length<>0 then begin
  assign(#,name_of_file); reset(#);
  if name_length=-1 then name_length:=file_name_size;
  end
@d reset_OK(#)==(name_length<>0) and (IOresult=0)
@d assign_write(#)==assign(#,name_of_file); rewrite(#)
@d rewrite_OK(#)==IOresult=0
@f external==begin
@d endexternal==
@f endexternal==end
@p function file_search(curdir,fname:shortstring;name_length:integer):shortstring;
external 'texext';
endexternal
@#
function a_open_in(var f:alpha_file):boolean;
@z

@x
begin reset(f,name_of_file,'/O'); a_open_in:=reset_OK(f);
@y
begin assign_read(f); a_open_in:=reset_OK(f);
@z

@x
begin rewrite(f,name_of_file,'/O'); a_open_out:=rewrite_OK(f);
@y
begin assign_write(f); a_open_out:=rewrite_OK(f);
@z

@x
begin reset(f,name_of_file,'/O'); b_open_in:=reset_OK(f);
@y
begin assign_read(f); b_open_in:=reset_OK(f);
@z

@x
begin rewrite(f,name_of_file,'/O'); b_open_out:=rewrite_OK(f);
@y
begin assign_write(f); b_open_out:=rewrite_OK(f);
@z

@x
begin reset(f,name_of_file,'/O'); w_open_in:=reset_OK(f);
@y
begin assign_read(f); w_open_in:=reset_OK(f);
@z

@x
begin rewrite(f,name_of_file,'/O'); w_open_out:=rewrite_OK(f);
@y
begin assign_write(f); w_open_out:=rewrite_OK(f);
@z

@x
begin if bypass_eoln then if not eof(f) then get(f);
  {input the first character of the line into |f^|}
@y
begin
@z

@x
  last:=last_nonblank; input_ln:=true;
@y
  read_ln(f); {this replaces the |bypass_eoln| mechanism}
  last:=last_nonblank; input_ln:=true;
@z

@x
is considered an output file the file variable is |term_out|.
@^system dependencies@>

@<Glob...@>=
@!term_in:alpha_file; {the terminal as an input file}
@!term_out:alpha_file; {the terminal as an output file}
@y
is considered an output file the file variable is |term_out|.
@^system dependencies@>

In ISO Pascal, |@!term_in| and |@!term_out| cannot be defined as typed files,
because that would prevent |read_ln| and |write_ln|. Instead, we define them
below as macros for |input| and |output|.
@z

@x
@d t_open_in==reset(term_in,'TTY:','/O/I') {open the terminal for text input}
@d t_open_out==rewrite(term_out,'TTY:','/O') {open the terminal for text output}
@y
@d term_in==INPUT {the terminal as an input file}
@d term_out==OUTPUT {the terminal as an output file}
@d t_open_in==reset(term_in) {open the terminal for text input}
@d t_open_out==rewrite(term_out) {open the terminal for text output}
@z

@x
@d update_terminal == break(term_out) {empty the terminal output buffer}
@d clear_terminal == break_in(term_in,true) {clear the terminal input buffer}
@y
@d update_terminal == flush(term_out) {empty the terminal output buffer}
@d clear_terminal == do_nothing {clear the terminal input buffer}
@z

@x
@p function init_terminal:boolean; {gets the terminal input started}
label exit;
begin t_open_in;
@y
@d argv==p@&a@&r@&a@&m@&s@&t@&r
@d argc==paramcount

@p function input_argv:boolean; {feed command line arguments into input}
var args:shortstring;
    i:integer;
    last_nonblank:0..buf_size; {|last| with trailing blanks removed}
begin
  if paramcount = 0 then input_argv:=false
  else begin
    last:=first; last_nonblank:=first;
    args:='';
    for i:=1 to argc do args:=args + argv(i) + ' ';
    for i:=1 to LENGTH(args) do begin
      if last>=max_buf_stack then
        begin max_buf_stack:=last+1;
        if max_buf_stack=buf_size then
          @<Report overflow of the input buffer, and abort@>;
        end;
      buffer[last]:=xord[args[i]]; incr(last);
      if buffer[last-1]<>" " then last_nonblank:=last;
    end;
    last:=last_nonblank; input_argv:=true;
  end;
end;
@#
function init_terminal:boolean; {gets the terminal input started}
label exit;
begin if input_argv then
  begin init_terminal:=true; loc:=first; end
  else begin t_open_in;
@z

@x
  write_ln(term_out,'Please type the name of your input file.');
  end;
exit:end;
@y
  write_ln(term_out,'Please type the name of your input file.');
  end;
end;
exit:end;
@z

@x
name_of_file:=pool_name; {we needn't set |name_length|}
@y
name_of_file:=pool_name;
name_length:=17; {we must set |name_length| for |file_search|}
@z

@x
@d max_halfword==65535 {largest allowable value in a |halfword|}
@y
@d max_halfword==@'7777777777 {largest allowable value in a |halfword|,
  enlarged from \TeX82's 65535}
@d max_trieword==65535 {\TeX82's value is kept for |trie_halves|}
@z

@x
@d hi(#)==#+min_halfword
  {to put a sixteen-bit item into a halfword}
@d ho(#)==#-min_halfword
  {to take a sixteen-bit item from a halfword}

@ The reader should study the following definitions closely:
@y
@d hi(#)==#+min_halfword
  {to put a 32-bit item into a halfword}
@d ho(#)==#-min_halfword
  {to take a 32-bit item from a halfword}

@ For the |trie| tables, we use a record type where |b0| and |b1| are
|0..max_trieword| rather than |quarterword| (as in \TeX82). Two of our
16-bit |trieword|s fit into a 32-bit |halfword|. This adds a fifth choice
to the |memory_word| type.

The reader should study the following definitions closely:
@z

@x
@<Types...@>=
@!quarterword = min_quarterword..max_quarterword; {1/4 of a word}
@y
@<Types...@>=
@!trieword = 0..max_trieword;
@!quarterword = min_quarterword..max_quarterword; {1/4 of a word}
@z

@x
@!four_choices = 1..4; {used when there are four variants in a record}
@y
@!five_choices = 1..5; {used when there are five variants in a record}
@z

@x
@!memory_word = record@;@/
  case four_choices of
  1: (@!int:integer);
  2: (@!gr:glue_ratio);
  3: (@!hh:two_halves);
  4: (@!qqqq:four_quarters);
  end;
@y
@!trie_halves = packed record@;@/
  @!rh:halfword;
  case two_choices of
  1: (@!lh:halfword); {one 32-bit |halfword|}
  2: (@!b0:trieword;@!b1:trieword); {two 16-bit |trieword|s}
  end;
@!memory_word = record@;@/
  case five_choices of
  1: (@!int:integer);
  2: (@!gr:glue_ratio);
  3: (@!hh:two_halves);
  4: (@!qqqq:four_quarters);
  5: (@!th:trie_halves);
  end;
@z

@x
print_int(w.qqqq.b2); print_char(":"); print_int(w.qqqq.b3);
@y
print_int(w.qqqq.b2); print_char(":"); print_int(w.qqqq.b3);@/
print_int(w.th.lh); print_char("="); print_int(w.th.b0); print_char(":");
print_int(w.th.b1); print_char(";"); print_int(w.th.rh); print_char(" ");
@z

@x
@p procedure fix_date_and_time;
begin sys_time:=12*60;
sys_day:=4; sys_month:=7; sys_year:=1776;  {self-evident truths}
@y
@p procedure cur_date_time(var t,d,m,y:integer);
external 'texext';
endexternal
@#
procedure fix_date_and_time;
begin
cur_date_time(sys_time,sys_day,sys_month,sys_year);
@z

@x
job_name_code: print(job_name);
@y
job_name_code: begin print(job_area);print(job_name);end;
@z

@x
@d TEX_area=="TeXinputs:"
@.TeXinputs@>
@d TEX_font_area=="TeXfonts:"
@y
@d TEX_area=="/tex/lib//" {\.{//} means all subdirectories}
@.TeXinputs@>
@d TEX_font_area=="/tex/fonts//"
@z

@x
  if (c=">")or(c=":") then
@y
  if c="/" then
@z

@x
@d format_default_length=20 {length of the |TEX_format_default| string}
@d format_area_length=11 {length of its area part}
@y
@d format_default_length=18 {length of the |TEX_format_default| string}
@d format_area_length=9 {length of its area part}
@z

@x
TEX_format_default:='TeXformats:plain.fmt';
@y
TEX_format_default:='/tex/fmt/plain.fmt';
@z

@x
@!job_name:str_number; {principal file name}
@y
@!job_name:str_number; {principal file name}
@!job_area:str_number; {directory name of principal file}
@!name_of_job_area:shortstring; {for passing to |file_search|}
@z

@x
begin cur_area:=""; cur_ext:=s;
@y
begin cur_area:=job_area; cur_ext:=s;
@z

@x
if job_name=0 then job_name:="texput";
@y
if job_name=0 then begin job_area:="";name_of_job_area:='';job_name:="texput";end;
@z

@x
@p procedure start_input; {\TeX\ will \.{\\input} something}
label done;
@y
@p procedure start_input; {\TeX\ will \.{\\input} something}
var j:pool_pointer;
label done;
@z

@x
  begin job_name:=cur_name; open_log_file;
@y
  begin job_area:=cur_area; name_of_job_area:='';
  for j:=str_start[job_area] to str_start[job_area+1]-1 do {pack
   |job_area| into shortstring}
    name_of_job_area:=name_of_job_area+xchr[so(str_pool[j])];
  job_name:=cur_name; open_log_file;
@z

@x
|if eof(tfm_file) then abort; end|\unskip'.
@^system dependencies@>

@d fget==get(tfm_file)
@y
|if eof(tfm_file) then abort; end|\unskip'.
@^system dependencies@>

@d fget==begin if eof(tfm_file) then abort;get(tfm_file);end
@z

@x
if eof(tfm_file) then abort;
for k:=np+1 to 7 do font_info[param_base[f]+k-1].sc:=0;
@y
{|eof(tfm_file)| is true after the last byte has been read}
for k:=np+1 to 7 do font_info[param_base[f]+k-1].sc:=0;
@z

@x
@!trie:array[trie_pointer] of two_halves; {|trie_link|, |trie_char|, |trie_op|}
@y
@!trie:array[trie_pointer] of trie_halves; {|trie_link|, |trie_char|, |trie_op|}
@z

@x
@!trie_used:array[ASCII_code] of quarterword;
@y
@!trie_used:array[ASCII_code] of trieword;
@z

@x
@!trie_op_val:array[1..trie_op_size] of quarterword;
@y
@!trie_op_val:array[1..trie_op_size] of trieword;
@z

@x
@!u:quarterword; {trial op code}
@y
@!u:trieword; {trial op code}
@z

@x
    if u=max_quarterword then
      overflow("pattern memory ops per language",
        max_quarterword-min_quarterword);
@y
    if u=max_trieword then
      overflow("pattern memory ops per language",
        max_trieword-min_quarterword);
@z

@x
@!h:two_halves; {template used to zero out |trie|'s holes}
@y
@!h:trie_halves; {template used to zero out |trie|'s holes}
@z

@x
@d dump_qqqq(#)==begin fmt_file^.qqqq:=#; put(fmt_file);@+end
@y
@d dump_qqqq(#)==begin fmt_file^.qqqq:=#; put(fmt_file);@+end
@d dump_th(#)==begin fmt_file^.th:=#; put(fmt_file);@+end
@z

@x
@d undump_qqqq(#)==begin get(fmt_file); #:=fmt_file^.qqqq;@+end
@y
@d undump_qqqq(#)==begin get(fmt_file); #:=fmt_file^.qqqq;@+end
@d undump_th(#)==begin get(fmt_file); #:=fmt_file^.th;@+end
@z

@x
for k:=0 to trie_max do dump_hh(trie[k]);
@y
for k:=0 to trie_max do dump_th(trie[k]);
@z

@x
for k:=0 to j do undump_hh(trie[k]);
@y
for k:=0 to j do undump_th(trie[k]);
@z

@x
  undump(min_quarterword)(max_quarterword)(hyf_next[k]);
@y
  undump(min_quarterword)(max_trieword)(hyf_next[k]);
@z

@x
if (x<>69069)or eof(fmt_file) then goto bad_fmt
@y
if (x<>69069) then goto bad_fmt {|eof(fmt_file)| is true after the last word
  has been read}
@z
