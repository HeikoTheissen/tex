@x
reset(pl_file);
@y
assign(pl_file,paramstr(1)+'.pl'); reset(pl_file);
@z

@x
@!tfm_file:packed file of 0..255;
@y
@!tfm_file:packed file of byte;
@z

@x
rewrite(tfm_file);
@y
assign(tfm_file,paramstr(1)+'.tfm'); rewrite(tfm_file);
@z

@x
@<Do the output@>;
@y
@<Do the output@>;
close(tfm_file); close(pl_file);
@z
